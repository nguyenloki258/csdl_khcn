using System.ComponentModel.DataAnnotations;

namespace CSDL_KHCN.Data_V2
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    public class BaseTableDefault 
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? LastModifiedOnDate { get; set; } = DateTime.Now;
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? CreatedOnDate { get; set; } = DateTime.Now;
    }
    public class BaseTable<T> where T : BaseTable<T>
    {
        #region Init Create/Update
        public T InitCreate()
        {
            return InitCreate(AppConstants.RootAppId,  UserConstants.AdministratorId);
        }
        public T InitCreate(Guid? application, Guid? userId)
        {
            return InitCreate(application ?? AppConstants.RootAppId, userId ??  UserConstants.AdministratorId);
        }
        public T InitCreate(Guid? application, Guid userId)
        {
            return InitCreate(application ?? AppConstants.RootAppId, userId);
        }
        public T InitCreate(Guid application, Guid? userId)
        {
            return InitCreate(application, userId ??  UserConstants.AdministratorId);
        }
        public T InitCreate(Guid application, Guid userId)
        {
            ApplicationId = application;
            CreatedByUserId = userId;
            LastModifiedByUserId = userId;
            CreatedOnDate = DateTime.Now;
            LastModifiedOnDate = DateTime.Now;
            return (T)this;
        }
        public T InitUpdate()
        {
            return InitUpdate(AppConstants.RootAppId,  UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid? application, Guid? userId)
        {
            return InitUpdate(application ?? AppConstants.RootAppId, userId ??  UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid? application, Guid userId)
        {
            return InitUpdate(application ?? AppConstants.RootAppId, userId);
        }
        public T InitUpdate(Guid application, Guid? userId)
        {
            return InitUpdate(application, userId ??  UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid application, Guid userId)
        {
            ApplicationId = application;
            LastModifiedByUserId = userId;
            LastModifiedOnDate = DateTime.Now;
            return (T)this;
        }
        #endregion
        public Guid CreatedByUserId { get; set; } = UserConstants.AdministratorId;
        public Guid LastModifiedByUserId { get; set; } = UserConstants.AdministratorId;
        public DateTime LastModifiedOnDate { get; set; } = DateTime.Now;
        public DateTime CreatedOnDate { get; set; } = DateTime.Now;
        [ForeignKey("Application")]
        public virtual Guid ApplicationId { get; set; } = AppConstants.RootAppId;
        public virtual SysApplication Application { get; set; }
    }


}
