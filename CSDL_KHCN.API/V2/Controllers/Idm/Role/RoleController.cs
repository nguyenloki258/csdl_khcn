﻿using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    /// <inheritdoc />
    /// <summary>
    /// Module nhóm người dùng
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/idm/roles")]
    [ApiExplorerSettings(GroupName = "04: IDM Roles")]
    public class IdmRoleController : ControllerBase
    {
        private readonly IRoleHandler _roleHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;
        public IdmRoleController(IRoleHandler roleHandler,  IUserMapRoleHandler userMapRoleHandler)
        {
            _roleHandler = roleHandler;
            _userMapRoleHandler = userMapRoleHandler;
        }

        #region CRUD

        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<RoleModel>), StatusCodes.Status200OK)]
        [SwaggerRequestExample(typeof(RoleCreateModel), typeof(MockupObject<RoleCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] RoleCreateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            if (!model.ApplicatonId.HasValue) model.ApplicatonId = appId;
            // Call service
            var result = await _roleHandler.CreateAsync(model, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        [SwaggerRequestExample(typeof(RoleUpdateModel), typeof(MockupObject<RoleUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] RoleUpdateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            if (!model.ApplicatonId.HasValue) model.ApplicatonId = appId;
             // Call service
             var result = await _roleHandler.UpdateAsync(id, model, appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _roleHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromBody]List<Guid> listId)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = applicationId??requestInfo.ApplicationId;
            // Call service 
            var result = await _roleHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<RoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _roleHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<RoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<RoleQueryModel>(filter);
            filterObject.Sort = sort;
            if (string.IsNullOrEmpty(filterObject.Sort))
            {
                filterObject.Sort = "+LastModifiedOnDate";
            }
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _roleHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<IList<RoleModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _roleHandler.GetAllAsync();
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion

        /// <summary>
        /// Lấy về chi tiết
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/detail")]
        [ProducesResponseType(typeof(ResponseObject<IList<RoleDetailModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetail(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _roleHandler.GetDetail(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách người dùng thuộc nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/user")]
        [ProducesResponseType(typeof(ResponseObject<IList<BaseUserModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserMapRoleAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.GetUserMapRoleAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        
 

        /// <summary>
        /// Gán người dùng vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/user")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddUserMapRoleAsync([FromBody]IList<Guid> listUserId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.AddUserMapRoleAsync(id, listUserId, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
 

        /// <summary>
        /// Gỡ người dùng khỏi nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/user")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteUserMapRoleAsync([FromBody]IList<Guid> listUserId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.DeleteUserMapRoleAsync(id, listUserId, appId);
            // Hander response
            return Helper.TransformData(result);
        }

    }
}
