﻿using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;

namespace CSDL_KHCN.Business_V2
{

    public class ThanhVienHoiDongKhoaHocModel
    {
        public Guid NhaKhoaHocId { get; set; }
        public string ChucVu { get; set; }
        public NhaKhoaHocModel NhaKhoaHoc { get; set; }
    }



    public class BaseHoiDongKhoaHocModel : BaseModel
    {
        public Guid Id { get; set; }
    }
    public class HoiDongKhoaHocModel : BaseHoiDongKhoaHocModel
    {
        public string Ten { get; set; }
        public string Ma { get; set; }
        public int TrangThai { get; set; }
        public int NamThanhLap { get; set; }
        public string FileDinhKem { get; set; }
        public IList<ThanhVienHoiDongKhoaHocModel> ThanhVienHoiDongKhoaHocs { get; set; }
    }
    public class HoiDongKhoaHocQueryModel : PaginationRequest
    {
        public int? TrangThai { get; set; }
        public int? NamThanhLap { get; set; }
    }

    public class HoiDongKhoaHocCreateModel
    {
        public string Ten { get; set; }
        public string Ma { get; set; }
        public int NamThanhLap { get; set; }
        public int TrangThai { get; set; }
        public string FileDinhKem { get; set; }
        public IList<ThanhVienHoiDongKhoaHocModel> ThanhVienHoiDongKhoaHocs { get; set; }
    }
    public class HoiDongKhoaHocUpdateModel
    {
        public Guid Id { get; set; }
        public string Ten { get; set; }
        public string Ma { get; set; }
        public int TrangThai { get; set; }
        public int NamThanhLap { get; set; }
        public string FileDinhKem { get; set; }
        public IList<ThanhVienHoiDongKhoaHocModel> ThanhVienHoiDongKhoaHocs { get; set; }
    }
}

