/* SAVIS Vietnam Corporation
 *
 * This module is to separate configuration of authentication to app.js
 * DuongPD - Jan 2018
 */

define(["jquery", "env"], function ($, env) {
    "use strict";

    /* Authorization configs 
     *  configure service based uri, client id, login and logout redirect urls
     *  we can also configure the time client will redirect to login page to re-new tokens
     */



    var sessionTimeoutConfig = {
        Enable: false,
        Mode: "idle", // session or idle 
        WarnAfter: 5000,
        RedirAfter: 60000
    }

    var initLogInUrl = function () {

        var url = env.FE_URL + "#!/login"  

        return url;
    };



    var clearLocalStorage = function () {

        window.localStorage.removeItem("access_token");
        window.localStorage.removeItem("session_state");
        window.localStorage.removeItem("user_id");
        window.localStorage.removeItem("display_name");
        window.localStorage.removeItem("app_id");
    };

    var factory;
    var sessionTimeOut = function () {
        var logOutUrl = "";
        var option = {
            keepAlive: false,
            warnAfter: sessionTimeoutConfig.WarnAfter,
            redirAfter: sessionTimeoutConfig.WarnAfter + sessionTimeoutConfig.RedirAfter,
            logoutUrl: logOutUrl,
            redirUrl: logOutUrl,
            logoutButton: "Đăng xuất",
            keepAliveButton: "Tiếp tục phiên",
            countdownSmart: true,
            countdownMessage: "Hệ thống sẽ tự động đăng xuất sau {timer}",
            countdownBar: true,
            onRedir: function () {
                factory.logOut();
            }
        };
        if (sessionTimeoutConfig.Mode === "session") {
            option.title = "Cảnh báo phiên làm việc";
            option.message = "Phiên làm việc của bạn đã hết. Chọn <strong>Đăng xuất</strong> hoặc <strong>Tiếp tục phiên</strong> để tiếp tục làm việc";
            option.ignoreUserActivity = true;
            option.isShowButton = true;
        } else if (sessionTimeoutConfig.Mode === "idle") {
            option.title = "Cảnh báo nhàn rỗi";
            option.message = "Không phát hiện hoạt động người dùng";
            option.ignoreUserActivity = false;
            option.isShowButton = false;
        }
        $.sessionTimeout(option);
    };

    factory = {};


    factory.getAuthenInfo = function () {
        var result = factory.checkAuthToken();
        // if (!result) {
        //     window.location = "/";
        // }
    };

    factory.initAuthConfig = function (app) {
        app.constant("ngAuthController", factory);
    };
    factory.initNonAuthConfig = function (app) {
        app.constant("ngAuthController", {});
    };
    factory.checkAuthToken = function () {

        var authData = window.localStorage["access_token"];
        var expiresTime = window.localStorage["expires_time"];
        // Check has value
        if (!authData || !expiresTime) return false;

        var timeExpires = new Date(expiresTime).getTime();
        var timeAtMoment = new Date().getTime();

        // Check time expires
        if (timeExpires < timeAtMoment) return false;

        return true;
    };

    factory.setupAuth = function (app) {
        // Setup session timeout
        if (sessionTimeoutConfig.Enable) sessionTimeOut();

        // This variable keeps dialog just open 1 time
        var isDialogOpened = false;

        var openDialog = function ($uibModal, url, message, title, buttonConfirm, buttonCancel, popupSize) {
            var infoDialogTemplateUrl = "/app-data/views/template/confirm/connect-expired.html" + app.Version;
            var modalInstance = $uibModal.open({
                templateUrl: infoDialogTemplateUrl,
                controller: "ConnectExpireDialogCtrl",
                size: popupSize,
                backdrop: "static",
                resolve: {
                    data: function () {
                        var obj = {};
                        obj.Url = url;
                        return obj;
                    },
                    option: function () {
                        return {
                            Message: message,
                            Title: title,
                            ButtonConfirm: buttonConfirm,
                            ButtonCancel: buttonCancel
                        };
                    }
                }
            });

            isDialogOpened = true;

            return modalInstance;
        };

        // Controller for displaying connect expired dialog
        app.controller("ConnectExpireDialogCtrl", ["$scope", "$timeout", "$uibModalInstance", "data", "option",
            function ($scope, $timeout, $uibModalInstance, data, option) {
                $scope.Title = option.Title;
                $scope.Message = option.Message;
                $scope.ButtonConfirm = option.ButtonConfirm;
                $scope.ButtonCancel = option.ButtonCancel;
                $scope.Url = data.Url;

                $scope.Confirm = function () {
                    $uibModalInstance.dismiss("cancel");
                }
                $timeout(function () {
                    window.location = $scope.Url;
                }, 1000);
            }
        ]);

        // inject http request to add bearer token
        app.factory("authInterceptorService", ["$q", "$injector", "$location", function ($q, $injector, $location) {

            var authInterceptorServiceFactory = {};
            var $uibModal;

            var request = function (config) {
                config.headers = config.headers || {};
                var authData = window.localStorage["access_token"];
                if (authData) {
                    config.headers.Authorization = "Bearer " + authData;
                }
                return config;
            };


            var responseError = function (rejection) {
                if (rejection.status === 401) {
                    $uibModal = $uibModal || $injector.get("$uibModal");

                    clearLocalStorage();
                    // Show dialog
                    var message = "Phiên xác thực của bạn không hợp lệ, vui lòng đăng nhập lại!";
                    if (!isDialogOpened) openDialog($uibModal, logOutUrl, message, "Cảnh báo", "", "", "");

                } else if (rejection.status === 403) {
                    $location.path("/forbidden");
                } else {}
                return rejection;
            };

            authInterceptorServiceFactory.request = request;
            authInterceptorServiceFactory.responseError = responseError;

            return authInterceptorServiceFactory;
        }]);

        app.config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push("authInterceptorService");
        }]);

        app.run(["$http", "$uibModal", "$rootScope", function ($http, $uibModal, $rootScope) {

            //Do your $on in here, like this:
            $rootScope.$on("$locationChangeStart", function () {

                $uibModal = $uibModal || $injector.get("$uibModal");
                //Do your things
                var authData = window.localStorage["access_token"];

                if (!authData) {

                    clearLocalStorage();

                    if (!isDialogOpened) openDialog($uibModal, url, "Phiên xác thực của bạn không hợp lệ", "Cảnh báo", "", "", "");

                }
            });
        }]);
    };

    factory.setupNonAuth = function (app) {
        // inject http request to add bearer token
        app.factory("authInterceptorService", ["$q", "$injector", "$location", function ($q, $injector, $location) {

            var authInterceptorServiceFactory = {};
            var $uibModal;

            var request = function (config) {
                config.headers = config.headers || {};
                var authData = window.localStorage["access_token"];
                if (authData) {
                    config.headers.Authorization = "Bearer " + authData;
                }
                return config;
            };


            var responseError = function (rejection) {
                return rejection;
            };

            authInterceptorServiceFactory.request = request;
            authInterceptorServiceFactory.responseError = responseError;

            return authInterceptorServiceFactory;
        }]);

        app.config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push("authInterceptorService");
        }]);
    };

    factory.toLoginForm = function () {
        var logInUrl = initLogInUrl();
        // Routing to login page
        window.location = logInUrl;
    }

    factory.logOut = function () {
        clearLocalStorage();
        location.reload();
    };

    factory.init = function () {
        factory.getAuthenInfo();
    };
    factory.init();

    return factory;
});