﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using CSDL_KHCN.Data_V2;
using Serilog;

namespace CSDL_KHCN.Business_V2
{
    public class RoleHandler : IRoleHandler
    {
        private readonly DbHandler<IdmRole, RoleModel, RoleQueryModel> _dbHandler =
            DbHandler<IdmRole, RoleModel, RoleQueryModel>.Instance;

        private readonly IUserMapRoleHandler _userMapRoleHandler;

        public RoleHandler(IUserMapRoleHandler userMapRoleHandler)
        {
            _userMapRoleHandler = userMapRoleHandler;
        }

        public RoleHandler()
        {
        }

        public async Task<Response> GetDetail(Guid id, Guid applicationId)
        {
            var model = await _dbHandler.FindAsync(id);

            if (model.Code == Code.Success)
            {
                var modelData = model as ResponseObject<RoleModel>;
                var result = AutoMapperUtils.AutoMap<RoleModel, RoleDetailModel>(modelData?.Data);
                var listUserResponse = await _userMapRoleHandler.GetUserMapRoleAsync(id, applicationId);
                    var listUserResponseData = model as ResponseObject<IList<BaseUserModel>>;
                    if (listUserResponseData != null) result.ListUser = listUserResponseData.Data;
                    return new ResponseObject<RoleDetailModel>(result);

            }

            return model;
        }

        public Task<Response> GetPageAsync(RoleQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public Task<Response> GetAllAsync(RoleQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<IdmRole>(true);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }

        private static Expression<Func<IdmRole, bool>> BuildQuery(RoleQueryModel query)
        {
            var predicate = PredicateBuilder.New<IdmRole>(true);
            if (!string.IsNullOrEmpty(query.Code)) predicate.And(s => s.Code == query.Code);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (query.Id.HasValue) predicate.And(s => s.Id == query.Id);
            if (query.ListId != null) predicate.And(s => query.ListId.Contains(s.Id));

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Code.Contains(query.FullTextSearch) || s.Name.Contains(query.FullTextSearch) ||
                    s.Description.Contains(query.FullTextSearch));
            return predicate;
        }

        #region CRUD

        public async Task<Response> CreateAsync(RoleCreateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RoleCreateModel, IdmRole>(model);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request, "Name", "Code");
            if (result.Code == Code.Success)
            {
                RoleCollection.Instance.LoadToHashSet();

                #region Realtive


                if (model.ListAddUserId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(request.Id, model.ListAddUserId,
                        model.ApplicatonId.Value,
                        appId, actorId);

                #endregion
            }

            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, RoleUpdateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RoleUpdateModel, IdmRole>(model);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request, "Name", "Code");
            if (result.Code == Code.Success)
            {
                RoleCollection.Instance.LoadToHashSet();

                #region Realtive


                if (model.ListAddUserId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(id, model.ListAddUserId, model.ApplicatonId.Value,
                        appId, actorId);
                if (model.ListDeleteUserId != null)
                    await _userMapRoleHandler.DeleteUserMapRoleAsync(id, model.ListDeleteUserId,
                        model.ApplicatonId.Value);

                #endregion
            }

            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentRole = await unitOfWork.GetRepository<IdmRole>().FindAsync(id);
                    if (currentRole == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại", null);
                    unitOfWork.GetRepository<IdmRole>().Delete(currentRole);
                    if (await unitOfWork.SaveAsync() > 0)
                    {
                        RoleCollection.Instance.LoadToHashSet();
                        return new ResponseDelete(id, currentRole.Name);
                    }

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var resultData = new List<ResponseDelete>();
            foreach (var id in listId)
            {
                var model = await DeleteAsync(id) as ResponseDelete;
                resultData.Add(model);
            }

            var result = new ResponseDeleteMulti(resultData);
            return result;
        }

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        #endregion
    }
}