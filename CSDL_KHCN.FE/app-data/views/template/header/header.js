define(['angularAMD'], function (angularAMD) {
    'use strict';
    angularAMD.controller('HeaderController', ['$scope', '$q', '$log', 'ngAuthController',
        '$http', 'ConstantsApp', 'UtilsService', "UserApiService", "ApplicationApiService",
        function ($scope, $q, $log, ngAuthController, $http, ConstantsApp, UtilsService, UserApiService, ApplicationApiService) {
            $scope.ValidateResourceUrl = UtilsService.ValidateResourceUrl;
            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
            });
            /* Declare variables url form popup add,edit,info */
            $scope.User = {};
            $scope.DropDownList = {};
            $scope.DropDownList.App = {
                Data: [],
                Model: null,
                Disabled: false
            };
            // $scope.ToggleHeader = function(){
            //     $(".top-menu").toggleClass("in")
            // }
            $scope.DropDownList.App.SelectedChange = function () {
                var appSelectedId = $scope.DropDownList.App.Model.id;
                window.localStorage['app_id'] = appSelectedId;
                window.localStorage['app_name'] = $scope.DropDownList.App.Model.name;
                window.location = "/";
            };

            /* ------------------------------------------------------------------------------- */

            /* BUTTON */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            // LOGOUT
            $scope.Button.LogOut = {};
            $scope.Button.LogOut.Click = function () {
                ngAuthController.logOut();
            };
            /* ------------------------------------------------------------------------------- */

            var isInitApp = false;
            var initAppAccessible = function () {
                var promise = ApplicationApiService.GetAccessibleByOwner();
                promise.then(function onSuccess(response) {
                    var dataResult = response.data;
                    $scope.DropDownList.App.Data = dataResult.data;
                    isInitApp = true;
                }, function onError(response) {
                    $log.error(response);
                    var message = "Tài khoản của bạn chưa được phân quyền truy cập vào hệ thống. Liên hệ Quản trị hệ thống để được hỗ trợ !";
                    var deniedResult = UtilsService.OpenDialog(message, 'Thông báo', 'Xác nhận', '', 'md', '');

                    deniedResult.result.then(function (modalResult) {
                        if (modalResult === 'confirm') {
                            ngAuthController.logOut();
                        }
                    });
                });
                return promise;
            };
            var loadUserInfo = function (userId) {
                var promise = UserApiService.GetById(userId);
                promise.then(function onSuccess(response) {
                    $log.debug(response);
                    var resultResponse = response.data;
                    $scope.User = resultResponse.data;
                }, function onError(response) {
                    $log.error(response);
                });
                return promise;
            };
            var init = function () {
                var appId = window.localStorage["app_id"];
                if (appId !== null && typeof (appId) !== 'undefined' && appId !== 'undefined' && !isInitApp) {
                    var userId = window.localStorage["user_id"];
                    var cmd = initAppAccessible();
                    var cmd2 = loadUserInfo(userId);
                    $q.all([cmd, cmd2]).then(function () {
                        $scope.DropDownList.App.Model = $scope.DropDownList.App.Data.filter(function (st) {
                            return st.id === appId
                        })[0];
                        if ($scope.DropDownList.App.Model === null || typeof $scope.DropDownList.App.Model === "undefined") {
                            $scope.DropDownList.App.Model = $scope.DropDownList.App.Data[0];
                            $scope.DropDownList.App.SelectedChange();
                        }
                        if ($scope.DropDownList.App.Data.length === 1) $scope.DropDownList.App.Disabled = true;
                        else $scope.DropDownList.App.Disabled = false;
                    });
                }
            };
            init();
        }
    ]);
});