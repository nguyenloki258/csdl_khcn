using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.IdentityModel.Tokens;

namespace CSDL_KHCN.API
{
    using System;
    using CSDL_KHCN.API.Constants;
    using Boxed.AspNetCore;
    using CorrelationId;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using CSDL_KHCN.Data_V2;
    using global::API.BasicAuth.Middlewares;

    /// <summary>
    /// The main start-up class for the application.
    /// </summary>
    public class Startup : IStartup
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment hostingEnvironment;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The application configuration, where key value pair settings are stored. See
        /// http://docs.asp.net/en/latest/fundamentals/configuration.html</param>
        /// <param name="hostingEnvironment">The environment the application is running under. This can be Development,
        /// Staging or Production by default. See http://docs.asp.net/en/latest/fundamentals/environments.html</param>
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            this.configuration = configuration;
            this.hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Configures the services to add to the ASP.NET Core Injection of Control (IoC) container. This method gets
        /// called by the ASP.NET runtime. See
        /// http://blogs.msdn.com/b/webdev/archive/2014/06/17/dependency-injection-in-asp-net-vnext.aspx
        /// </summary>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            AppConstants.EnvironmentName = this.hostingEnvironment.EnvironmentName;

            if (this.configuration["Authencate:Jwt:Enable"] == "true")
            {
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = configuration["Authencate:Jwt:Issuer"],
                            ValidAudience = configuration["Authencate:Jwt:Issuer"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Authencate:Jwt:Key"]))
                        };
                    });
            }
            else
            {
                services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = "Custom Scheme";
                        options.DefaultChallengeScheme = "Custom Scheme";
                    })
                    .AddCustomAuth(o => { });
            }
            return services
                .AddCorrelationIdFluent()
                .AddCustomCaching()
                .AddCustomOptions(this.configuration)
                .AddCustomRouting()
                .AddResponseCaching()
                .AddCustomResponseCompression()
                .AddCustomSwagger(this.configuration)
                .AddCustomStrictTransportSecurity()
                .AddHttpContextAccessor()
                // Add useful interface for accessing the HttpContext outside a controller.
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                // Add useful interface for accessing the IUrlHelper outside a controller.
                .AddScoped(x => x
                    .GetRequiredService<IUrlHelperFactory>()
                    .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext))
                .AddCustomApiVersioning()
                .AddMvcCore()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddApiExplorer()
                .AddAuthorization()
                .AddDataAnnotations()
                .AddJsonFormatters()
                .AddCustomJsonOptions(this.hostingEnvironment)
                .AddCustomCors(this.configuration)
                .AddVersionedApiExplorer(x =>
                    {
                        x.GroupNameFormat = "'v'VVV";
                        // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                        // can also be used to control the format of the API version in route templates
                        x.SubstituteApiVersionInUrl = true;
                    })

                // Version format: 'v'major[.minor][-status]
                .AddCustomMvcOptions(this.hostingEnvironment)
                .Services
                .AddProjectServices()
                .BuildServiceProvider();
        }

        /// <summary>
        /// Configures the application and HTTP request pipeline. Configure is called after ConfigureServices is
        /// called by the ASP.NET runtime.
        /// </summary>
        public void Configure(IApplicationBuilder application) =>
            application
                // Pass a GUID in a X-Correlation-ID HTTP header to set the HttpContext.TraceIdentifier.
                .UseCorrelationId()
                .UseForwardedHeaders()
                .UseResponseCaching()
                .UseResponseCompression()
                .UseCors(CorsPolicyName.AllowAny)
                .UseDeveloperErrorPages()
                // .UseIf(
                //     this.hostingEnvironment.IsDevelopment(),
                //     x => x.UseDeveloperErrorPages())
                .UseStaticFilesWithCacheControl()
                .UseAuthentication()
                .UseIf(
                    this.configuration["Cors:AllowAll"] == "true",
                    x => x.UseCors(CorsPolicyName.AllowAny))
                .UseIf(
                    this.configuration["Cors:AllowAll"] == "true",
                    x => x.UseCors(CorsPolicyName.AllowFrontEnd).UseCors(CorsPolicyName.AllowThirdparty))
                .UseMvc()
                .UseSwagger()
                .UseSwaggerUI(
                    options =>
                    {
                        // options.DefaultModelExpandDepth(2);
                        options.DefaultModelRendering(ModelRendering.Example);
                        // options.DefaultModelsExpandDepth(-1);
                        options.DisplayOperationId();
                        options.DisplayRequestDuration();
                        options.EnableDeepLinking();
                        //                        options.MaxDisplayedTags(5);
                        // options.ShowExtensions();
                        //options.EnableValidator();
                        // options.SupportedSubmitMethods(SubmitMethod.Get, SubmitMethod.Head);
                        options.EnableFilter();
                        options.DocExpansion(DocExpansion.None);
                        var provider = application.ApplicationServices.GetService<IApiVersionDescriptionProvider>();
                        foreach (var apiVersionDescription in provider
                            .ApiVersionDescriptions
                            .OrderByDescending(x => x.ApiVersion).ThenByDescending(s => s.GroupName))
                        {
                            options.SwaggerEndpoint(
                                $"/swagger/{apiVersionDescription.GroupName}/swagger.json",
                                $"Version {apiVersionDescription.ApiVersion}");
                        }
                        options.InjectStylesheet("/swagger.css");


                    });
    }
}