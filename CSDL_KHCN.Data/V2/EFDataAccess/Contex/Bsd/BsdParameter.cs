namespace CSDL_KHCN.Data_V2
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("bsd_Parameter")]
    public  class BsdParameter : BaseTable<BsdParameter>
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
        [StringLength(1024)]
        public string Description { get; set; }
        [StringLength(128)]
        public string Value { get; set; }

    }
}
