define(["angularAMD", "env"], function (angularAMD, env) {
    'use strict';
    var ConsApp = {};
    /** API_URL */
    ConsApp.BASED_API_URL = env.API_URL;
    ConsApp.API_STATIC_FILE = env.STATIC_FILE_URL;
    ConsApp.FileCatalystPartitionId = "7924ccf8-f47f-4188-866d-3ba009a0aec8";
    ConsApp.API_APPLICATION_URL = "/api/v2/sys/applications";
    /** */
    ConsApp.API_JWT_URL = "/api/v2/jwt";
    ConsApp.API_ROLE_URL = "/api/v2/idm/roles";
    ConsApp.API_USER_URL = "/api/v2/idm/users";
    ConsApp.API_PARAMETER_URL = "/api/v2/bsd/parameters";
    ConsApp.API_NODE_URL = "/api/v2/core/nodes";

    ConsApp.API_NHAKHOAHOC_URL = "/api/v2/bus/nha_khoa_hoc";

    ConsApp.API_HOIDONGKHOAHOC_URL = "/api/v2/bus/hoi_dong_khoa_hoc";
    

    ConsApp.GRID_ATTRIBUTES = {
        PageNumber: 1,
        PageSize: 10,
        DataCount: 0,
        TotalCount: 0,
        FromRecord: 0,
        ToRecord: 0,
        Data: [],
        Check: false,
        GrantAccess: true,
        PageSizeOptions: [5, 10, 25, 50],
        GetHeaderText: function (key, header) {
            for (var i = 0; i < header.length; i++) {
                var element = header[i];
                if (element.Key === key)
                    return element.Value + ": ";

            }
        }
    };



    ConsApp.LinhVucList = [
        { Name: "Khoa học Tự nhiên"},
        { Name: "Khoa học Xã hội"},
        { Name: "Khoa học Nhân văn"},
        { Name: "Khoa học Nông nghiệp"},
        { Name: "Khoa học Y dược"},
        { Name: "Khoa học Y Kỹ thuật và Công Nghệ"},
    ];
    ConsApp.ChucDanhNghienCuuList = [
        { Name: "Chuyên viên"},
        { Name: "Chuyên viên chính"},
        { Name: "Chuyên viên cao cấp"},
        { Name: "Chuyên gia"},
    ];

    ConsApp.HocHamList = [
        { Name: "Giáo sư"},
        { Name: "Phó giáo sư"},
    ];
    ConsApp.HocViList = [
        { Name: "Cử nhân" },
        { Name: "Kỹ sư" },
        { Name: "Thạc sĩ" },
        { Name: "Tiến sĩ" },
    ];


    angularAMD.constant("ConstantsApp", ConsApp);
});