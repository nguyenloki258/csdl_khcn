define(['angularAMD'], function (angularAMD) {

    /***
    GLobal Directives
    ***/

    // Route State Load Spinner(used on page or content load)
    angularAMD.directive('ngSpinnerBar', ['$rootScope', '$state',
        function ($rootScope, $state) {
            return {
                controller: ['$element', function ($element) {
                    // by defult hide the spinner bar
                    $element.addClass('hide'); // hide spinner bar by default

                    // display the spinner bar whenever the route changes(the content part started loading)
                    $rootScope.$on('$stateChangeStart', function () {
                        $element.removeClass('hide'); // show spinner bar
                    });

                    // hide the spinner bar on rounte change success(after the content loaded)
                    $rootScope.$on('$stateChangeSuccess', function (event) {
                        $element.addClass('hide'); // hide spinner bar
                        $('body').removeClass('page-on-load'); // remove page loading indicator
                        Layout.setAngularJsSidebarMenuActiveLink('match', null, event.currentScope.$state); // activate selected link in the sidebar menu

                        // auto scorll to page top
                        setTimeout(function () {
                            App.scrollTop(); // scroll to the top on content load
                        }, $rootScope.settings.layout.pageAutoScrollOnLoad);
                    });

                    // handle errors
                    $rootScope.$on('$stateNotFound', function () {
                        $element.addClass('hide'); // hide spinner bar
                    });

                    // handle errors
                    $rootScope.$on('$stateChangeError', function () {
                        $element.addClass('hide'); // hide spinner bar
                    });
                }]
            };
        }
    ])

    // Handle global LINK click
    angularAMD.directive('a', function () {
        return {
            restrict: 'E',
            controller: ['$element', '$attrs', function ($element, $attrs) {
                if ($attrs.ngClick || $attrs.href === '' || $attrs.href === '#') {
                    $element.on('click', function (e) {
                        e.preventDefault(); // prevent link click for above criteria
                    });
                }
            }]
        };
    });

    angularAMD.directive('timer', ['$compile', function ($compile) {
        return {
            restrict: 'EAC',
            replace: false,
            scope: {
                interval: '=interval',
                startTimeAttr: '=startTime',
                endTimeAttr: '=endTime',
                countdownattr: '=countdown',
                finishCallback: '&finishCallback',
                autoStart: '&autoStart',
                maxTimeUnit: '='
            },
            controller: ['$scope', '$element', '$attrs', '$timeout', function ($scope, $element, $attrs, $timeout) {

                // Checking for trim function since IE8 doesn't have it
                // If not a function, create tirm with RegEx to mimic native trim
                if (typeof String.prototype.trim !== 'function') {
                    String.prototype.trim = function () {
                        return this.replace(/^\s+|\s+$/g, '');
                    };
                }

                //angular 1.2 doesn't support attributes ending in "-start", so we're
                //supporting both "autostart" and "auto-start" as a solution for
                //backward and forward compatibility.
                $scope.autoStart = $attrs.autoStart || $attrs.autostart;

                if ($element.html().trim().length === 0) {
                    $element.append($compile('<span>{{millis}}</span>')($scope));
                } else {
                    $element.append($compile($element.contents())($scope));
                }

                $scope.startTime = null;
                $scope.endTime = null;
                $scope.timeoutId = null;
                $scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) >= 0 ? parseInt($scope.countdownattr, 10) : undefined;
                $scope.isRunning = false;

                $scope.$on('timer-start', function () {
                    $scope.start();
                });

                $scope.$on('timer-resume', function () {
                    $scope.resume();
                });

                $scope.$on('timer-stop', function () {
                    $scope.stop();
                });

                $scope.$on('timer-clear', function () {
                    $scope.clear();
                });

                $scope.$on('timer-set-countdown', function (e, countdown) {
                    $scope.countdown = countdown;
                });

                function resetTimeout() {
                    if ($scope.timeoutId) {
                        clearTimeout($scope.timeoutId);
                    }
                }

                $scope.start = $element[0].start = function () {
                    $scope.startTime = $scope.startTimeAttr ? new Date($scope.startTimeAttr) : new Date();
                    $scope.endTime = $scope.endTimeAttr ? new Date($scope.endTimeAttr) : null;
                    if (!$scope.countdown) {
                        $scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) > 0 ? parseInt($scope.countdownattr, 10) : undefined;
                    }
                    resetTimeout();
                    tick();
                    $scope.isRunning = true;
                };

                $scope.resume = $element[0].resume = function () {
                    resetTimeout();
                    if ($scope.countdownattr) {
                        $scope.countdown += 1;
                    }
                    $scope.startTime = new Date() - ($scope.stoppedTime - $scope.startTime);
                    tick();
                    $scope.isRunning = true;
                };

                $scope.stop = $scope.pause = $element[0].stop = $element[0].pause = function () {
                    var timeoutId = $scope.timeoutId;
                    $scope.clear();
                    $scope.$emit('timer-stopped', {
                        timeoutId: timeoutId,
                        millis: $scope.millis,
                        seconds: $scope.seconds,
                        minutes: $scope.minutes,
                        hours: $scope.hours,
                        days: $scope.days
                    });
                };

                $scope.clear = $element[0].clear = function () {
                    // same as stop but without the event being triggered
                    $scope.stoppedTime = new Date();
                    resetTimeout();
                    $scope.timeoutId = null;
                    $scope.isRunning = false;
                };

                $element.bind('$destroy', function () {
                    resetTimeout();
                    $scope.isRunning = false;
                });

                function calculateTimeUnits() {
                    if ($attrs.startTime !== undefined) {
                        $scope.millis = new Date() - new Date($scope.startTimeAttr);
                    }
                    // compute time values based on maxTimeUnit specification
                    if (!$scope.maxTimeUnit || $scope.maxTimeUnit === 'day') {
                        $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
                        $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
                        $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
                        $scope.days = Math.floor((($scope.millis / (3600000)) / 24));
                        $scope.months = 0;
                        $scope.years = 0;
                    } else if ($scope.maxTimeUnit === 'second') {
                        $scope.seconds = Math.floor($scope.millis / 1000);
                        $scope.minutes = 0;
                        $scope.hours = 0;
                        $scope.days = 0;
                        $scope.months = 0;
                        $scope.years = 0;
                    } else if ($scope.maxTimeUnit === 'minute') {
                        $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
                        $scope.minutes = Math.floor($scope.millis / 60000);
                        $scope.hours = 0;
                        $scope.days = 0;
                        $scope.months = 0;
                        $scope.years = 0;
                    } else if ($scope.maxTimeUnit === 'hour') {
                        $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
                        $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
                        $scope.hours = Math.floor($scope.millis / 3600000);
                        $scope.days = 0;
                        $scope.months = 0;
                        $scope.years = 0;
                    } else if ($scope.maxTimeUnit === 'month') {
                        $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
                        $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
                        $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
                        $scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
                        $scope.months = Math.floor((($scope.millis / (3600000)) / 24) / 30);
                        $scope.years = 0;
                    } else if ($scope.maxTimeUnit === 'year') {
                        $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
                        $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
                        $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
                        $scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
                        $scope.months = Math.floor((($scope.millis / (3600000)) / 24 / 30) % 12);
                        $scope.years = Math.floor(($scope.millis / (3600000)) / 24 / 365);
                    }
                    // plural - singular unit decision
                    $scope.secondsS = ($scope.seconds === 1 || $scope.seconds === 0) ? '' : 's';
                    $scope.minutesS = ($scope.minutes === 1 || $scope.minutes === 0) ? '' : 's';
                    $scope.hoursS = ($scope.hours === 1 || $scope.hours === 0) ? '' : 's';
                    $scope.daysS = ($scope.days === 1 || $scope.days === 0) ? '' : 's';
                    $scope.monthsS = ($scope.months === 1 || $scope.months === 0) ? '' : 's';
                    $scope.yearsS = ($scope.years === 1 || $scope.years === 0) ? '' : 's';
                    //add leading zero if number is smaller than 10
                    $scope.sseconds = $scope.seconds < 10 ? '0' + $scope.seconds : $scope.seconds;
                    $scope.mminutes = $scope.minutes < 10 ? '0' + $scope.minutes : $scope.minutes;
                    $scope.hhours = $scope.hours < 10 ? '0' + $scope.hours : $scope.hours;
                    $scope.ddays = $scope.days < 10 ? '0' + $scope.days : $scope.days;
                    $scope.mmonths = $scope.months < 10 ? '0' + $scope.months : $scope.months;
                    $scope.yyears = $scope.years < 10 ? '0' + $scope.years : $scope.years;

                }

                //determine initial values of time units and add AddSeconds functionality
                if ($scope.countdownattr) {
                    $scope.millis = $scope.countdownattr * 1000;

                    $scope.addCDSeconds = $element[0].addCDSeconds = function (extraSeconds) {
                        $scope.countdown += extraSeconds;
                        $scope.$digest();
                        if (!$scope.isRunning) {
                            $scope.start();
                        }
                    };

                    $scope.$on('timer-add-cd-seconds', function (e, extraSeconds) {
                        $timeout(function () {
                            $scope.addCDSeconds(extraSeconds);
                        });
                    });

                    $scope.$on('timer-set-countdown-seconds', function (e, countdownSeconds) {
                        if (!$scope.isRunning) {
                            $scope.clear();
                        }

                        $scope.countdown = countdownSeconds;
                        $scope.millis = countdownSeconds * 1000;
                        calculateTimeUnits();
                    });
                } else {
                    $scope.millis = 0;
                }
                calculateTimeUnits();

                var tick = function () {

                    $scope.millis = new Date() - $scope.startTime;
                    var adjustment = $scope.millis % 1000;

                    if ($scope.endTimeAttr) {
                        $scope.millis = $scope.endTime - new Date();
                        adjustment = $scope.interval - $scope.millis % 1000;
                    }


                    if ($scope.countdownattr) {
                        $scope.millis = $scope.countdown * 1000;
                    }

                    if ($scope.millis < 0) {
                        $scope.stop();
                        $scope.millis = 0;
                        calculateTimeUnits();
                        if ($scope.finishCallback) {
                            $scope.$eval($scope.finishCallback);
                        }
                        return;
                    }
                    calculateTimeUnits();

                    //We are not using $timeout for a reason. Please read here - https://github.com/siddii/angular-timer/pull/5
                    $scope.timeoutId = setTimeout(function () {
                        tick();
                        $scope.$digest();
                    }, $scope.interval - adjustment);

                    $scope.$emit('timer-tick', {
                        timeoutId: $scope.timeoutId,
                        millis: $scope.millis
                    });

                    if ($scope.countdown > 0) {
                        $scope.countdown--;
                    } else if ($scope.countdown <= 0) {
                        $scope.stop();
                        if ($scope.finishCallback) {
                            $scope.$eval($scope.finishCallback);
                        }
                    }
                };

                if ($scope.autoStart === undefined || $scope.autoStart === true) {
                    $scope.start();
                }
            }]
        };
    }]);

    angularAMD.directive('httpSrc', ['$http', function ($http) {
        return {
            restrict: 'A',
            priority: 2,
            controller: ['$attrs', function ($attrs) {
                var requestConfig = {
                    method: 'GET',
                    responseType: 'arraybuffer',
                    cache: 'true',
                    ignoreLoadingBar: true,
                };

                function base64Img(data) {
                    var arr = new Uint8Array(data.data);
                    var raw = '';
                    var i, j, subArray, chunk = 5000;
                    for (i = 0, j = arr.length; i < j; i += chunk) {
                        subArray = arr.subarray(i, i + chunk);
                        raw += String.fromCharCode.apply(null, subArray);
                    }
                    return btoa(raw);
                };

                function getcontentType(data) {

                    var headers = data.headers();

                    return headers['content-type'];
                };
                $attrs.$observe('httpSrc', function (newValue) {
                    if (newValue !== $attrs["old-http-src"]) {
                        $attrs.$set('old-http-src', newValue);

                        if (newValue.indexOf("http://") !== 0 && newValue.indexOf("https://") !== 0) {
                            $attrs.$set('src', newValue);
                        } else {
                            requestConfig.url = newValue;
                            $http(requestConfig)
                                .then(function (data) {
                                    if ($attrs["controlType"] === "jwplayer") {
                                        $attrs.$set('video', "data:" + getcontentType(data) + ";base64," + base64Img(data));
                                    } else {
                                        $attrs.$set('src', "data:" + getcontentType(data) + ";base64," + base64Img(data));
                                    }
                                });
                        }
                    }
                });
            }]
        };
    }]);

    angularAMD.directive('dateutc', function () {
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$formatters.push(function (value) {
                    try {
                        var date = new Date(Date.parse(value));
                        date = new Date(date.getTime());
                        return date;
                    } catch (e) {
                        return null;
                    }
                });

                ctrl.$parsers.push(function (value) {
                    if (value !== null && typeof (value) !== "undefined") {
                        var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                        return date;
                    } else {
                        return null;
                    }

                });
            }
        };
    });

    angularAMD.directive('jwplayerDirective', [function () {
        return {
            restrict: 'EAC',
            priority: 1,
            link: function ($scope, elm, $attrs) {
                // jwplayer.key = "CKjOe06GxAOe3Dj9NaWPCQKtqvqQdyFV8z9wsg==";
                $attrs.$observe('video', function (newValue) {
                    if (newValue) {
                        var authData = window.localStorage["access_token"];
                        jwplayer($attrs.id).setup({
                            width: $attrs.width,
                            height: $attrs.height,
                            primary: $attrs.primary,
                            type: $attrs.type,
                            file: $attrs.video,
                            onXhrOpen: function (xhr, url) {
                                xhr.setRequestHeader('Authorization', "Bearer " + authData);
                            }
                        });
                    }
                });
            }
        }
    }]);
    angularAMD.filter('highlightfts', function () {
        function removeVietNamSign(str) {
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            //str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
            //str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1- 
            // str = str.replace(/^\-+|\-+$/g, "");
            return str;
        }
        return function (matchItem, query) {
            if (query && matchItem) {
                var queryNS = removeVietNamSign(query);
                var matchItemNS = removeVietNamSign(matchItem);
                //get các vị trí
                var listItem = matchItemNS.split(queryNS);
                var result = "";
                var fromIndex = 0
                var toIndex = 0
                for (var i = 0; i < listItem.length; i++) {
                    fromIndex = toIndex;
                    toIndex = fromIndex + listItem[i].length;
                    result = result + matchItem.substring(fromIndex, toIndex);
                    fromIndex = toIndex;
                    if (i !== listItem.length - 1) {
                        result = result + '<span class="highlight-fts">';
                        toIndex = fromIndex + queryNS.length;
                        result = result + matchItem.substring(fromIndex, toIndex);
                        result = result + '</span>';
                    }
                }
                return result;
            } else {
                return matchItem;
            }
        };
    })
});